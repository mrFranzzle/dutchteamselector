package com.cgi.workshops.swf.dutchteamselector.service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cgi.workshops.swf.dutchteamselector.model.SearchCriteria;
import com.cgi.workshops.swf.dutchteamselector.model.SoccerPlayer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


@Service("soccerTeamService")
public class SoccerTeamServiceImpl implements SoccerTeamService {
	
	public List<SoccerPlayer> retrieveAllSoccerPlayers() {
		return new ArrayList<SoccerPlayer>(fetchAllSoccerPlayers());
	}

	public SoccerPlayer selectPlayerWithId(final String id,final List<SoccerPlayer> allSoccerPlayers) {
		SoccerPlayer sp = new SoccerPlayer();
		sp.setId(id);
		return allSoccerPlayers.get(allSoccerPlayers.indexOf(sp));
	}

	public List<SoccerPlayer> findAllSoccerPlayersWithName(SearchCriteria searchCriteria) {
		final Collection<SoccerPlayer> soccerPlayers = fetchAllSoccerPlayers();
		final List<SoccerPlayer> foundPlayersWithKeywordInTheName = new ArrayList<SoccerPlayer>();
		
		for(SoccerPlayer soccerPlayer : soccerPlayers){
			if(soccerPlayer != null){
				String wildcardSearchCriteria = String.format(".*%s.*", searchCriteria.getKeyword());
				
				if(soccerPlayer.getDisplayName() != null && soccerPlayer.getDisplayName().toLowerCase().matches(wildcardSearchCriteria.toLowerCase())){
					foundPlayersWithKeywordInTheName.add(soccerPlayer);
				}
			}
		}
		
		return foundPlayersWithKeywordInTheName;
	}
	
	private Collection<SoccerPlayer> fetchAllSoccerPlayers() {
		InputStream is = SoccerTeamService.class.getResourceAsStream("huidigeSpelers.json");
		
		Reader reader = readerForInputStream(is);
		Gson gson = new Gson();
		
		Type collectionType = new TypeToken<Collection<SoccerPlayer>>(){}.getType();
		
		Collection<SoccerPlayer> players = gson.fromJson(reader, collectionType);
		
		return players;
	}
	
	private Reader readerForInputStream(InputStream inputStream){
		try {
			return new InputStreamReader(inputStream, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
}
