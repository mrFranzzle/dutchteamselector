package com.cgi.workshops.swf.dutchteamselector.service;

import java.util.List;

import com.cgi.workshops.swf.dutchteamselector.model.SearchCriteria;
import com.cgi.workshops.swf.dutchteamselector.model.SoccerPlayer;

public interface SoccerTeamService {
	
	/**
	 * Retrieves all soccerplayers eligable to play in the dutch footballteam.
	 */
	List<SoccerPlayer> retrieveAllSoccerPlayers();
	
	/**
	 * Select the player from allSoccerPlayers with the correct id.
	 * @param id
	 * @return
	 */
	SoccerPlayer selectPlayerWithId(String id, List<SoccerPlayer> allSoccerPlayers);
	
	
	/**
	 * Returns all dutch soccer players.
	 * Returns those that have a first or lastname that matches any anyPartOfSoccerPlayersName
	 * @param anyPartOfSoccerPlayersName 
	 * @return all dutch soccer players.
	 */
	List<SoccerPlayer> findAllSoccerPlayersWithName(final SearchCriteria searchCriteria);
}
