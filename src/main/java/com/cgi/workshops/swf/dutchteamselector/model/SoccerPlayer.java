package com.cgi.workshops.swf.dutchteamselector.model;

import java.io.Serializable;

import org.springframework.stereotype.Component;

/**
 * Describes a player.
 * This class should implement Serializable because Spring Webflow stores the state of the model,
 * between state transitions.
 * @author kramerf
 *
 */
@Component("soccerPlayer")
public class SoccerPlayer implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private String firstName;
	private String lastName;
	private int caps;
	private int minutes;
	private int goals;
	
	private PlayerType playerType;

	
	/**
	 * Returns the identifier of the soceerPlayer.
	 * @return the identifier of the soceerPlayer.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the identifier of the soceerPlayer.
	 * @param id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Returns the firstname of the soccerPlayer.
	 * @return the firstname of the soccerPlayer.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the firstname of the soceerPlayer.
	 * @param firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Returns the lastName of the soccerPlayer.
	 * @return the lastName of the soccerPlayer.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the lastName of the soccerPlayer.
	 * @param lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Returns the playerType of the soccerPlayer.
	 * @return the playerType of the soccerPlayer.
	 */
	public PlayerType getPlayerType() {
		return playerType;
	}

	/**
	 * Sets the playerType of the soccerPlayer.
	 * @param playerType to set
	 */
	public void setPlayerType(PlayerType playerType) {
		this.playerType = playerType;
	}
	
	/**
	 * Returns the amount of caps the soccerplayer played ('interlands' in dutch).
	 * @return the amount of caps the soccerplayer played ('interlands' in dutch).
	 */
	public int getCaps() {
		return caps;
	}

	/**
	 * Sets the amount of caps the soccerplayer played ('interlands' in dutch).
	 * @param caps to set
	 */
	public void setCaps(int caps) {
		this.caps = caps;
	}

	/**
	 * Returns the number of minutes the player was in the field. 
	 * @return the number of minutes the player was in the field
	 */
	public int getMinutes() {
		return minutes;
	}

	/**
	 * Sets the number of minutes the player was in the field
	 * @param minutes to set
	 */
	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	/**
	 * Returns the number of goals the player made.
	 * @return the number of goals the player made.
	 */
	public int getGoals() {
		return goals;
	}

	/**
	 * Sets the number of goals the player made.
	 * @param goals to set
	 */
	public void setGoals(int goals) {
		this.goals = goals;
	}

	/**
	 * Returns the displayname (e.g. 'Daley, Blind')
	 * @return the displayname (e.g. 'Daley, Blind')
	 */
	public String getDisplayName(){
		return String.format("%s %s", firstName, lastName);
	}

	/**
	 * Overridden for debug purposes.
	 */
	@Override
	public String toString() {
		final String format = "id:%s,firstName:%s,lastName:%s,caps:%d,minutes:%d,goals:%d,playerType:%s";
		
		return String.format(format,id, firstName, lastName, caps, minutes, goals, playerType);
	}

	@Override
	public int hashCode() {
		if(id == null){
			return 0;
		}else{
			return id.hashCode();	
		}
	}

	@Override
	public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        SoccerPlayer that = (SoccerPlayer) other;
        return that.id.equals(this.id);
	}
}
