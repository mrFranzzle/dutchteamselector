package com.cgi.workshops.swf.dutchteamselector.handler;

import org.springframework.webflow.mvc.servlet.AbstractFlowHandler;

public class SelectDutchTeamHandler extends AbstractFlowHandler{

    /**
     * The starting the flow name prefix (see onderhoudenCV.xml defined in WEB-INF/config/portlet-onderhouden-cv-config.xml
     * @return the starting flow name prefix (see onderhoudenCV.xml defined in WEB-INF/config/portlet-onderhouden-cv-config.xml
     */
    public String getFlowId() {
    	return "selectBestSoccerTeam-flow";
    }
}