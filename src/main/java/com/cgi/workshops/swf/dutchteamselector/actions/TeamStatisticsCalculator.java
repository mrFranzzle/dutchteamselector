package com.cgi.workshops.swf.dutchteamselector.actions;

import com.cgi.workshops.swf.dutchteamselector.model.SoccerPlayer;
import com.cgi.workshops.swf.dutchteamselector.model.SoccerTeamSelection;
import com.cgi.workshops.swf.dutchteamselector.model.TeamStatistics;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Action to calculates the statistics of the team.
 */
@Component
public class TeamStatisticsCalculator {

    /**
     * Calculates the statistics of the team.
     * @param soccerTeamSelection to calculate the statistics for
     * @return the statistics of the team.
     */
	public TeamStatistics calculateStatistics(SoccerTeamSelection soccerTeamSelection) {
		TeamStatistics teamStatistics = new TeamStatistics();

        int experienceInCaps = 0;
        int experienceInMinutes = 0;
        int effectivenessInGoals = 0;

        List<SoccerPlayer> selectedPlayers = soccerTeamSelection.getSelectedPlayers();
        for(SoccerPlayer soccerPlayer : selectedPlayers){
            experienceInCaps += soccerPlayer.getCaps();
            experienceInMinutes += soccerPlayer.getMinutes();
            effectivenessInGoals += soccerPlayer.getGoals();
        }

        teamStatistics.setExperienceInCaps(experienceInCaps);
        teamStatistics.setExperienceInMinutes(experienceInMinutes);
        teamStatistics.setEffectivenessInGoals(effectivenessInGoals);

        return teamStatistics;
	}
	
}
