package com.cgi.workshops.swf.dutchteamselector.validation;

import com.cgi.workshops.swf.dutchteamselector.model.SoccerTeamSelection;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.binding.message.MessageResolver;
import org.springframework.stereotype.Component;


/**
 * Validates the selection of players
 */
@Component("soccerTeamSelectionValidator")
public class SoccerTeamSelectionValidator {

    /**
     * Validates the soccerTeamSelection
     * @param soccerTeamSelection to validate
     * @param messageContext to put the message and to check if the soccerTeamSelection is valid
     * @return true if the soccerTeamSelection is valid, false otherwise
     */
    public boolean validateSoccerTeamSelection(SoccerTeamSelection soccerTeamSelection, MessageContext messageContext){
        //TODO Implementeer validaties, voorbeelden
        // Heeft het team het maximum aantal spelers bereikt?
        // Zijn er spelers dubbel geselecteerd?
        // Is er meer dan 1 keeper geselecteerd?

        if(soccerTeamSelection.getSelectedPlayers().size() > 22){
            MessageResolver resolvedMessage = new MessageBuilder().error().defaultText("Te veel spelers geselecteerd.").build();

            messageContext.addMessage(resolvedMessage);
        }

        return !messageContext.hasErrorMessages();
    }

}
