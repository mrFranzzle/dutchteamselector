package com.cgi.workshops.swf.dutchteamselector.model;

import java.io.Serializable;

public class TeamStatistics implements Serializable {

    int experienceInCaps;
    int experienceInMinutes;
    int effectivenessInGoals;

    /**
     * Returns experience of the team (the total number of caps).
     * @return the total number of caps.
     */
    public int getExperienceInCaps() {
        return experienceInCaps;
    }

    /**
     * Sets the total number of caps.
     * @param experienceInCaps to set
     */
    public void setExperienceInCaps(int experienceInCaps) {
        this.experienceInCaps = experienceInCaps;
    }

    /**
     * Returns the experience of the team measured in minutes.
     * @return the experience of the team measured in minutes.
     */
    public int getExperienceInMinutes() {
        return experienceInMinutes;
    }

    /**
     * Sets the experience of the team measured in minutes.
     * @param experienceInMinutes to set
     */
    public void setExperienceInMinutes(int experienceInMinutes) {
        this.experienceInMinutes = experienceInMinutes;
    }

    /**
     * Returns the effectiveness of your team measured in goals
     * @return the effectiveness of your team measured in goals
     */
    public int getEffectivenessInGoals() {
        return effectivenessInGoals;
    }

    /**
     * Sets the effectiveness of your team measured in goals
     * @param effectivenessInGoals
     */
    public void setEffectivenessInGoals(int effectivenessInGoals) {
        this.effectivenessInGoals = effectivenessInGoals;
    }
}
