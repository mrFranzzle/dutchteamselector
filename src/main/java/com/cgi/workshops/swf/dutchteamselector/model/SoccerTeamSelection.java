package com.cgi.workshops.swf.dutchteamselector.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

/**
 * Holds all the selected players.
 */
@Component("soccerTeamSelection")
public class SoccerTeamSelection implements Serializable {
	private static final long serialVersionUID = -5145124885660252876L;
	
	private List<SoccerPlayer> selectedPlayers = new ArrayList<SoccerPlayer>();

	public List<SoccerPlayer> getSelectedPlayers() {
		return selectedPlayers;
	}

	public void addSoccerPlayer(SoccerPlayer soccerPlayer){
		selectedPlayers.add(soccerPlayer);
	}
}
