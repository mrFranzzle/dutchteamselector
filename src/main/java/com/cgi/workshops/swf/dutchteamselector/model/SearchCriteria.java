package com.cgi.workshops.swf.dutchteamselector.model;

import java.io.Serializable;

/**
 *
 * This class should implement Serializable because Spring Webflow stores the state of the model,
 * between state transitions.
 * @author kramerf
 *
 */
public class SearchCriteria implements Serializable {
	private static final long serialVersionUID = 8633619851688020608L;
	private String keyword;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	
}
