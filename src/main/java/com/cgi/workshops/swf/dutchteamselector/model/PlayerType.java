package com.cgi.workshops.swf.dutchteamselector.model;

/**
 * PlayerType defines the type of player.
 */
public enum PlayerType {
    MIDFIELDER("label.midfielder",0), 
    ATTACKER("label.attacker",1), 
    DEFENDER("label.defender",2),
    KEEPER("label.keeper",3);
    
    private String messageKey;
    private int code;

    /**
     * Constructs a PlayerType enum for a given messageKey and code. 
     * @param messageKey resolves to a description
     * @param code for comparison between two PlayerType enumeration values
     */
    PlayerType(String messageKey, int code) {
        this.messageKey = messageKey;
        this.code = code;
    }

    /**
     * Returns the messageKey that can be resolved to a description of the playertype.
     * (e.g. 'label.midfielder' can be resolved to 'Middenvelder')
     * @return the messageKey that can be resolved to a description of the playertype.
     */
	public String getMessageKey() {
		return messageKey;
	}

	/**
	 * Returns code to be able to compare PlayerType enumeration values
	 * @return code to be able to compare PlayerType enumeration values
	 */
	public int getCode() {
		return code;
	}
}
