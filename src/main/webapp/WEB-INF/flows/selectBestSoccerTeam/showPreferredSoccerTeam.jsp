<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<HTML>
	<head>
		<title>Dutch Team Selector</title>
		<c:set var="styleUrl"><c:url value="/resources/style.css" /></c:set>
		<link type="text/css" rel="stylesheet" href="${styleUrl }">
	</head> 
	<body>

		<div id="leftcol">
			<c:url value="/resources/knvb.png" var="kvnbImageUrl" />
			<img src="${kvnbImageUrl}" /> 
		</div>
		<div id="main">
			<div id="rightcol">

			</div>
			<div id="centercol">
				<h1>Selectie</h1>
				<div class="spelers">
					<c:forEach items="${ preferredSoccerTeam.selectedPlayers }" var="soccerPlayer">
						<div class="speler">${ soccerPlayer.displayName }</div>
					</c:forEach>
				</div>
				
				
				<h1>Team statistieken</h1>
                <div><label>Totaal aantal interlands : </label>${teamStatistics.experienceInCaps}</div>
                <div><label>Totaal aantal minuten : </label>${teamStatistics.experienceInMinutes}</div>
                <div><label>Totaal aantal goals : </label>${teamStatistics.effectivenessInGoals}</div>
			
				<c:url context="/" value="${flowExecutionUrl}" var="backToSearch">
					<c:param name="_eventId">backToSearch</c:param>
				</c:url>			 	
				<h2><a href="${backToSearch }">Terug om te zoeken</a></h2>
			
			</div>
		</div>
		<br id="footerbr" />
		<div id="footer"><h6>franzzle.com Copyright</h6></div>
	</body>
</html>	
		