<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<HTML>
	<head>
		<title>Dutch Team Selector</title>
		<c:set var="styleUrl"><c:url value="/resources/style.css" /></c:set>
		<link type="text/css" rel="stylesheet" href="${styleUrl }">
	</head> 
	<body>
		<div id="leftcol">
			<c:url value="/resources/knvb.png" var="kvnbImageUrl" />
			<img src="${kvnbImageUrl}" /> 
		</div>
		<div id="main">
			<div id="rightcol"><h1>Selectie</h1>
				<div class="spelers">
					<c:forEach items="${ preferredSoccerTeam.selectedPlayers }" var="soccerPlayer">
						<div class="speler">${ soccerPlayer.displayName }</div>
					</c:forEach>
				</div>
				<c:url context="/" value="${flowExecutionUrl}" var="backToSearch">
					<c:param name="_eventId">showPreferred</c:param>
				</c:url>			 	
				<h2><a href="${backToSearch }">Toon Beste Team</a></h2>
			</div>
			<div id="centercol">
				<form:form method="POST" modelAttribute="searchCriteria">
					<input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}"/>
					<form:errors path="*" />
					<div class="centered">
						<label>Zoek spelers</label>
						<form:input cssClass="zoekArgument" path="keyword"></form:input>
						<input type="submit" class="button" name="_eventId_search" value="Search"/>
					</div>
				</form:form>
				<table>
				<c:forEach items="${ foundSoccerPlayers }" var="soccerPlayer">
					<tr class="player">
						<c:url value="/resources/playerBlack.png" var="playerBlackImageUrl" />
						<td class="label"><img src="${playerBlackImageUrl}" /></td>
						
						<c:url context="/" value="${flowExecutionUrl}" var="selectPlayerUrl">
							<c:param name="_eventId">select</c:param>
							<c:param name="playerId">${soccerPlayer.id}</c:param>
						</c:url>
						
						<td class="name"><a href="${selectPlayerUrl}"> ${ soccerPlayer.firstName } ${ soccerPlayer.lastName }</a></td>
					</tr>
				</c:forEach>
				</table>
			</div>
		</div>
		<br id="footerbr" />
		<div id="footer"><h6>franzzle.com Copyright</h6></div>
	</body>

</html>