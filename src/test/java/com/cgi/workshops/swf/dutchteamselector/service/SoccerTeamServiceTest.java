package com.cgi.workshops.swf.dutchteamselector.service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.util.StringUtils;

import com.cgi.workshops.swf.dutchteamselector.model.PlayerType;
import com.cgi.workshops.swf.dutchteamselector.model.SearchCriteria;
import com.cgi.workshops.swf.dutchteamselector.model.SoccerPlayer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Test for {@link SoccerTeamService}
 * @author kramerf
 */
public class SoccerTeamServiceTest {
	
	@Test
	public void selectPlayerWithId(){
		SoccerTeamService soccerTeamService = new SoccerTeamServiceImpl();
		
		List<SoccerPlayer> soccerPlayers = soccerTeamService.retrieveAllSoccerPlayers();
		
		Assert.assertNotNull(soccerPlayers);
		Assert.assertFalse(soccerPlayers.isEmpty());
		
		SoccerPlayer sp = soccerTeamService.selectPlayerWithId("1", soccerPlayers);
		
		Assert.assertNotNull(sp);
		Assert.assertEquals("1", sp.getId());
		Assert.assertEquals("Patrick", sp.getFirstName());
	}
	
	@Test
	public void findAllSoccerPlayersWithNameForKeyword(){
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setKeyword("sne");
		
		SoccerTeamService soccerTeamService = new SoccerTeamServiceImpl();
		List<SoccerPlayer> soccerPlayers = soccerTeamService.findAllSoccerPlayersWithName(searchCriteria);
		
		Assert.assertFalse(soccerPlayers.isEmpty());
		SoccerPlayer soccerPlayer = soccerPlayers.get(0);
		Assert.assertEquals("Wesley", soccerPlayer.getFirstName());
	}

	@Test
	public void findAllSoccerPlayersWithNameForKeywordNull(){
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setKeyword(null);
		
		SoccerTeamService soccerTeamService = new SoccerTeamServiceImpl();
		List<SoccerPlayer> soccerPlayers = soccerTeamService.findAllSoccerPlayersWithName(searchCriteria);
		
		Assert.assertTrue(soccerPlayers.isEmpty());
	}
	
	@Ignore
	@Test
	public void findGson() throws Exception{
		List<SoccerPlayer> soccerPlayers = new ArrayList<SoccerPlayer>();
		
		InputStream is = SoccerTeamServiceTest.class.getResourceAsStream("spelers.json");
		Reader reader = new InputStreamReader(is);
		
		
		Gson gson = new Gson();
		
		Type collectionType = new TypeToken<Collection<PlayerFromWeb>>(){}.getType();
		
		Collection<PlayerFromWeb> players = gson.fromJson(reader, collectionType);

		for(PlayerFromWeb playerFromWeb : players){
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Date debut = simpleDateFormat.parse(playerFromWeb.getDebuut());
			Calendar instance = Calendar.getInstance();
			instance.setTime(debut);
			int year = instance.get(Calendar.YEAR);
			
			if(year > 1998){
				addSoccerPlayer(soccerPlayers, playerFromWeb);	
			}
			
		}

		Integer id = 1;
		for(SoccerPlayer soccerPlayer : soccerPlayers){
			soccerPlayer.setId(String.format("%s",id));
			System.out.println(gson.toJson(soccerPlayer) + ",");
			id++;
		}
		
	}

	@Ignore
	@Test
	public void findGsonSoccerPlayers() throws Exception{
		InputStream is = SoccerTeamServiceTest.class.getResourceAsStream("huidigeSpelers.json");
		Reader reader = new InputStreamReader(is);
		Gson gson = new Gson();
		
		Type collectionType = new TypeToken<Collection<SoccerPlayer>>(){}.getType();
		
		Collection<SoccerPlayer> players = gson.fromJson(reader, collectionType);
	
		for(SoccerPlayer soccerPlayer : players){
			System.out.println(soccerPlayer.toString());
		}
		
	}
	
	
	private void addSoccerPlayer(List<SoccerPlayer> soccerPlayers,
			PlayerFromWeb playerFromWeb) {
		SoccerPlayer soccerPlayer = newSoccerPlayer(soccerPlayers);
		String name = StringUtils.trimLeadingWhitespace(playerFromWeb.getNaam());
		String[] nameParts = name.split(" ");
		soccerPlayer.setFirstName(nameParts[0]);
		
		StringBuilder stringBuilder = new StringBuilder();
		for(int i = 1; i < nameParts.length; i++){
			stringBuilder.append(nameParts[i]);
			stringBuilder.append(" ");
		}
		if(playerFromWeb.getPositie().equalsIgnoreCase("Verdediger")){
			soccerPlayer.setPlayerType(PlayerType.DEFENDER);
		}else if (playerFromWeb.getPositie().equalsIgnoreCase("Aanvaller")){
			soccerPlayer.setPlayerType(PlayerType.ATTACKER);
		}else if (playerFromWeb.getPositie().equals("Doelman")){
			soccerPlayer.setPlayerType(PlayerType.KEEPER);
		}else if (playerFromWeb.getPositie().equalsIgnoreCase("Middenvelder")){
			soccerPlayer.setPlayerType(PlayerType.MIDFIELDER);
		}
		
		soccerPlayer.setCaps(playerFromWeb.getInterlands());
		soccerPlayer.setGoals(playerFromWeb.getGoals());
		soccerPlayer.setMinutes(playerFromWeb.getMinuten());
		
		soccerPlayer.setLastName(StringUtils.trimTrailingWhitespace(stringBuilder.toString()));
	}
	
	private SoccerPlayer newSoccerPlayer(final List<SoccerPlayer> soccerPlayers){
		SoccerPlayer soccerPlayer = new SoccerPlayer();

		soccerPlayers.add(soccerPlayer);
		return soccerPlayer;
	}
	
    public static String getPathForClass(Class<?> clazz){
        final String name = clazz.getPackage().getName();
        return "/" + StringUtils.replace(name, ".", "/") + "/";
    }
}
