package com.cgi.workshops.swf.dutchteamselector.service;

public class PlayerFromWeb {
	private String display;
	private String naam;
	private int interlands;
	private int minuten;
	private int goals;
	private int aanvoerder;
	private String debuut;
	private String laatst;
	private String positie;
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	public String getNaam() {
		return naam;
	}
	public void setNaam(String naam) {
		this.naam = naam;
	}
	public int getInterlands() {
		return interlands;
	}
	public void setInterlands(int interlands) {
		this.interlands = interlands;
	}
	public int getMinuten() {
		return minuten;
	}
	public void setMinuten(int minuten) {
		this.minuten = minuten;
	}
	public int getGoals() {
		return goals;
	}
	public void setGoals(int goals) {
		this.goals = goals;
	}
	public int getAanvoerder() {
		return aanvoerder;
	}
	public void setAanvoerder(int aanvoerder) {
		this.aanvoerder = aanvoerder;
	}
	public String getDebuut() {
		return debuut;
	}
	public void setDebuut(String debuut) {
		this.debuut = debuut;
	}
	public String getLaatst() {
		return laatst;
	}
	public void setLaatst(String laatst) {
		this.laatst = laatst;
	}
	public String getPositie() {
		return positie;
	}
	public void setPositie(String positie) {
		this.positie = positie;
	}
	
	
	
	
}
